op = ""
loop do
    puts "Escolha a operação:"
    puts "1. Adição"
    puts "2. Subtração"
    puts "3. Multiplicação"
    puts "4. Divisão"
    puts "0. Sair"
    op = gets.chomp.to_i

    case op
    when 1
        puts "Digite um número por vez para Somar: "
        num1 = gets.chomp.to_i
        num2 = gets.chomp.to_i
        puts "------------------"
        puts "Resultado: #{num1 + num2}"
    when 2
        puts "Digite um número por vez para Subtrair: "
        num1 = gets.chomp.to_i
        num2 = gets.chomp.to_i
        puts "------------------"
        puts "Resultado: #{num1 - num2}"
    when 3
        puts "Digite um número por vez para Multiplicação: "
        num1 = gets.chomp.to_i
        num2 = gets.chomp.to_i
        puts "------------------"
        puts "Resultado: #{num1 * num2}"
    when 4
        puts "Digite um número por vez para Dividir: "
        num1 = gets.chomp.to_i
        num2 = gets.chomp.to_i
        puts "------------------"
        if num2 != 0
            div = num1 / num2.to_f
            puts "Resultado: #{div}"
        else
            puts "Erro: divisão por zero"
        end
    when 0
        puts "Saindo do programa."
        break
    else
        puts "Operação inválida"
        break
    end
end